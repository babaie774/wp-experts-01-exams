<?php

class Image
{

    public static function __callStatic($method, $args)
    {
        $methodName = 'get' . ucfirst($method);
        if (method_exists('Image', $methodName) && is_callable("Image::" . $methodName)) {
            return call_user_func("Image::" . $methodName, ...$args);
        }
    }

    private static function getExtension($image_file)
    {
        return pathinfo($image_file, PATHINFO_EXTENSION);
    }

    private static function getMime(string $image_file)
    {
        return mime_content_type($image_file);
    }

    private static function getSize($image_file)
    {
        return filesize($image_file);
    }


}

echo Image::extension(__FILE__);
